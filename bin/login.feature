Feature: Login for leaftap application
#Background:
#Given open the browser
#And Maximize the browser
#And launch the browser
Scenario Outline: positive login flow
And enter the username as <username>
And enter the password as <password>
When click the login
When click the crm/sfa
When click lead tab
When click create lead tab
And enter company name as <companyname>
And enter first name as <firstname>
And enter last name as <lastname>
When click the final create lead
Then verify create lead is success
Examples:
|username|password|companyname|firstname|lastname|
|DemoSalesManager|crmsfa|cts|uma|s|

