package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class ViewleadPage extends Annotations{
	public ViewleadPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID, using="viewLead_statusId_sp") WebElement eleverify;
	@And("verify create lead is success")
	public ViewleadPage verifyStatus() {
		 verifyDisplayed(eleverify);
		return this;
		 
	}

}
