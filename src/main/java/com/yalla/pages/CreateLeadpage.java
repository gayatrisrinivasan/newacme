package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class CreateLeadpage extends Annotations {
  
	public CreateLeadpage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement elecname;
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement elefname;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement elelname;
	@FindBy(how=How.NAME, using="submitButton") WebElement elecreatelead;
	@And("enter company name as (.*)")
	public CreateLeadpage enterCompanyName(String data) {
		//WebElement eleUserName = locateElement("id", "username");
		clearAndType(elecname, data);  
		return this; 
	}
	@And("enter first name as (.*)")
	public CreateLeadpage enterFirstName(String data) {
		//WebElement eleUserName = locateElement("id", "username");
		clearAndType(elefname, data);  
		return this; 
	}
	@And("enter last name as (.*)")
	public CreateLeadpage enterLastName(String data) {
		//WebElement eleUserName = locateElement("id", "username");
		clearAndType(elelname, data);  
		return this; 
	}
	@And("click the final create lead")
	public ViewleadPage clickCreateLead()
	{
		click(elecreatelead);
		return new ViewleadPage();
	}
}
