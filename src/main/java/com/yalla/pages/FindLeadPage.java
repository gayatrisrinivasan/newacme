package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeadPage extends Annotations {
	public FindLeadPage()
	{
		PageFactory.initElements(driver, this);

	}
	@FindBy(how=How.LINK_TEXT, using="Find Leads") WebElement elefindlead;
	@FindBy(how=How.XPATH, using="//span[text()='Phone']") WebElement elephoneid;
	@FindBy(how=How.NAME, using="phoneNumber") WebElement elephone;
    @FindBy(how=How.XPATH, using="//button[text()='Find Leads']") WebElement eleclickfl;

	public FindLeadPage clickCreateLead(String data ) {
		click(elefindlead);
		click(elephoneid);
		clearAndType(elephone,data);
		click(eleclickfl);
		return this;
	}
}


