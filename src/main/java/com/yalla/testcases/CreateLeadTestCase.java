package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.CreateLeadpage;
import com.yalla.pages.HomePage;
import com.yalla.pages.LoginPage;
import com.yalla.pages.MyHomePage;
import com.yalla.pages.MyLeadsPage;
import com.yalla.pages.ViewleadPage;
import com.yalla.testng.api.base.Annotations;

public class CreateLeadTestCase extends Annotations {
	@BeforeTest
	public void setData() {
		testcaseName = "CreateLeadTestCase";
		testcaseDec = "Login into leaftaps";
		author = "Gayatri";
		category = "smoke";
		excelFileName = "TC001";
	} 

	@Test(dataProvider="fetchData") 
	public void loginAndLogout(String uName, String pwd, String fname,String lname,String cname ) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin();
		new HomePage().clickCrm();
		new MyHomePage().clickLead();
		new MyLeadsPage().clickCreateLead();
		new CreateLeadpage()
		.enterCompanyName(cname).enterFirstName(fname).enterLastName(lname).clickCreateLead();
		new ViewleadPage()
		.verifyStatus();
		

}
}
